FROM docker.alatvala.fi/devops/jenkins-slave:latest

RUN apt-get update && apt-get install -y --no-install-recommends python-pip python-setuptools python-wheel && apt-get clean && pip install -U setuptools wheel platformio
